﻿using AutoMapper;

namespace File_Storage.BLL.AutoMapper
{
    public class AutoMapperConfigurationBll
    {
        readonly AutoMapperProfileBll _profile;
        public AutoMapperConfigurationBll(AutoMapperProfileBll profile)
        {
            if (profile is null)
            {
                _profile = new AutoMapperProfileBll();
            }
            else
            {
                _profile = profile;
            }
        }
        public MapperConfiguration InitializeAutoMapper()
        {
            MapperConfiguration config = new MapperConfiguration(cfg => cfg.AddProfile(_profile));

            return config;
        }
    }
}
