﻿using AutoMapper;
using File_Storage.BLL.DTO;
using File_Storage.DAL.Entities;
using System.Linq;

namespace File_Storage.BLL.AutoMapper
{
    public class AutoMapperProfileBll : Profile
    {
        public AutoMapperProfileBll()
        {
            CreateMap<FileEntity, FileDto>()
                .ReverseMap();

            CreateMap<ApplicationUser, UserDto>()
                .ForMember(dest => dest.Role, t => t.MapFrom(src => src.Roles.FirstOrDefault()))
                .ReverseMap();

            CreateMap<FileDto, FileEntity>()
                .ReverseMap();

            CreateMap<ExceptionDetailDto, ExceptionDetail>()
                .ReverseMap();
        }
    }
}
