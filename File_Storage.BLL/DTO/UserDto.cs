﻿using System.Collections.Generic;

namespace File_Storage.BLL.DTO
{
    public class UserDto
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public string Role { get; set; }
        public IList<FileDto> FileIdentityModels { get; set; }

    }
}
