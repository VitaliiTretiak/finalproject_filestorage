﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace File_Storage.BLL.Interfaces
{
    public interface ICrud<TModel> where TModel : class
    {
        IEnumerable<TModel> GetAll();
        TModel GetById(string id);
        Task Create(TModel item);
        void Update(TModel item);
        void Delete(string id);
    }
}
