﻿using File_Storage.BLL.DTO;
using System.Collections.Generic;

namespace File_Storage.BLL.Interfaces
{
    public interface IExceptionService
    {
        IEnumerable<ExceptionDetailDto> GetAll();
        void Create(ExceptionDetailDto exception);

    }
}
