﻿using File_Storage.BLL.DTO;
using System;
using System.Threading.Tasks;

namespace File_Storage.BLL.Interfaces
{
    public interface IFileService : ICrud<FileDto>, IDisposable
    {
        Task SoftDelete(string id);
    }
}
