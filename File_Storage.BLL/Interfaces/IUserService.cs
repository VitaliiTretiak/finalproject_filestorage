﻿using File_Storage.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace File_Storage.BLL.Interfaces
{
    public interface IUserService : ICrud<UserDto>, IDisposable
    {
        IList<string> GetRoles(string id);
        ClaimsIdentity Authenticate(UserDto userDto);
    }
}
