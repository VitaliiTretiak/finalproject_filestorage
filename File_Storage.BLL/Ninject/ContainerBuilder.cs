﻿using AutoMapper;
using File_Storage.BLL.AutoMapper;
using File_Storage.BLL.Interfaces;
using File_Storage.BLL.Services;
using File_Storage.DAL.Interfaces;
using File_Storage.DAL.Repositories;
using Ninject.Modules;

namespace File_Storage.BLL.Ninject
{
    public class ContainerBuilder : NinjectModule
    {
        readonly AutoMapperConfigurationBll _config;
        public ContainerBuilder(AutoMapperConfigurationBll config)
        {
            if (config is null)
            {
                _config = new AutoMapperConfigurationBll(new AutoMapperProfileBll());
            }
            else
            {
                _config = config;
            }
        }
        public override void Load()
        {
            Bind<IUnitOfWork>().To<UnitOfWork>().WithConstructorArgument("DefaultConnection");
            Bind<IFileService>().To<FileService>();
            Bind<IUserService>().To<UserService>();
            Bind<IExceptionService>().To<ExceptionService>();

            var mapper = _config.InitializeAutoMapper().CreateMapper();
            Bind<IMapper>().ToConstant(mapper);
        }
    }

}
