﻿using AutoMapper;
using File_Storage.BLL.DTO;
using File_Storage.BLL.Interfaces;
using File_Storage.DAL.Entities;
using File_Storage.DAL.Interfaces;
using System.Collections.Generic;

namespace File_Storage.BLL.Services
{
    public class ExceptionService : IExceptionService
    {
        readonly IUnitOfWork _unitOfWork;
        readonly IMapper _mapper;

        public ExceptionService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public IEnumerable<ExceptionDetailDto> GetAll()
        {
            return _mapper.Map<IEnumerable<ExceptionDetail>, IEnumerable<ExceptionDetailDto>>(_unitOfWork.ExceptionDetailRepository.GetAll());
        }

        public void Create(ExceptionDetailDto exception)
        {
            ExceptionDetail exceptionEntity = _mapper.Map<ExceptionDetailDto, ExceptionDetail>(exception);

            _unitOfWork.ExceptionDetailRepository.Create(exceptionEntity);
            _unitOfWork.Save();
        }
    }
}
