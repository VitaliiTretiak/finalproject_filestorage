﻿using AutoMapper;
using File_Storage.BLL.DTO;
using File_Storage.BLL.Interfaces;
using File_Storage.DAL.Entities;
using File_Storage.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace File_Storage.BLL.Services
{
    public class FileService : IFileService
    {
        readonly IUnitOfWork _unitOfWork;
        readonly IMapper _mapper;

        public FileService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task Create(FileDto item)
        {
            FileEntity fileEntity = _mapper.Map<FileDto, FileEntity>(item);

            await _unitOfWork.FileRepository.Create(fileEntity);
            await _unitOfWork.Save();
        }

        public async Task SoftDelete(string id)
        {
            await _unitOfWork.FileRepository.SoftDelete(id);
            await _unitOfWork.Save();
        }
        public void Delete(string id)
        {
            _unitOfWork.FileRepository.Delete(id);
        }

        public FileDto GetById(string id)
        {
            return _mapper.Map<FileEntity, FileDto>(_unitOfWork.FileRepository.GetById(id));
        }

        public IEnumerable<FileDto> GetAll()
        {
            return _mapper.Map<IEnumerable<FileEntity>, IEnumerable<FileDto>>(_unitOfWork.FileRepository.GetAll());
        }

        public void Update(FileDto item)
        {
            _unitOfWork.FileRepository.Update(_mapper.Map<FileDto, FileEntity>(item));
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                disposedValue = true;
            }
        }
        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
