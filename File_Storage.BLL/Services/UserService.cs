﻿using AutoMapper;
using File_Storage.BLL.DTO;
using File_Storage.BLL.Interfaces;
using File_Storage.DAL.Entities;
using File_Storage.DAL.Interfaces;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace File_Storage.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        readonly IMapper _mapper;

        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task Create(UserDto item)
        {
            ApplicationUser user = _unitOfWork.UserManager.FindByEmail(item.Email);
            if (user == null)
            {
                user = new ApplicationUser { Email = item.Email, UserName = item.Email };
                var result = _unitOfWork.UserManager.Create(user, item.Password);

                if (!result.Errors.Any())
                    _unitOfWork.UserManager.AddToRole(user.Id, item.Role);

                await _unitOfWork.Save();
            }
        }

        public void Update(UserDto item)
        {
            _unitOfWork.UserManager.Update(_mapper.Map<UserDto, ApplicationUser>(item));
        }


        public UserDto GetById(string id)
        {
            return _mapper.Map<ApplicationUser, UserDto>(_unitOfWork.UserManager.Get(id));
        }

        public IEnumerable<UserDto> GetAll()
        {
            return _mapper.Map<IEnumerable<ApplicationUser>, IEnumerable<UserDto>>(_unitOfWork.UserManager.GetAll());
        }

        public IList<string> GetRoles(string id)
        {
            return _unitOfWork.UserManager.GetRoles(id);
        }

        public void Delete(string id)
        {
            var user = _unitOfWork.UserManager.Get(id);
            _unitOfWork.UserManager.Delete(user);
        }

        public ClaimsIdentity Authenticate(UserDto userDto)
        {
            ClaimsIdentity claim = null;

            ApplicationUser user = _unitOfWork.UserManager.Find(userDto.Email, userDto.Password);

            if (user != null)
                claim = _unitOfWork.UserManager.CreateIdentity(user,
                                            DefaultAuthenticationTypes.ApplicationCookie);
            return claim;
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                disposedValue = true;
            }
        }
        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
