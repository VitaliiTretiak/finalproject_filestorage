﻿using System;
using System.Runtime.Serialization;

namespace File_Storage.BLL.Validation
{
    [Serializable]
    public sealed class FileStorageException : Exception, ISerializable
    {
        public FileStorageException() { }
        public FileStorageException(string message) : base(message) { }
        private FileStorageException(SerializationInfo info, StreamingContext context) : base(info, context) { }
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }

}
