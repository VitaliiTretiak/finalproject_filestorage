﻿using File_Storage.DAL.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace File_Storage.DAL.EF
{
    public class ApplicationContext : IdentityDbContext<ApplicationUser>
    {
        static ApplicationContext()
        {
            Database.SetInitializer(new ApplicationContextInitializer());
        }
        public ApplicationContext() { }
        public ApplicationContext(string connectionString)
            : base(connectionString)
        {
        }

        public DbSet<FileEntity> Files { get; set; }
        public DbSet<ExceptionDetail> ExceptionDetails { get; set; }
    }
}
