﻿using File_Storage.DAL.Entities;
using File_Storage.DAL.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;

namespace File_Storage.DAL.EF
{
    public class ApplicationContextInitializer : CreateDatabaseIfNotExists<ApplicationContext>
    {
        protected override void Seed(ApplicationContext context)
        {
            var roleManager = new ApplicationRoleManager(new RoleStore<ApplicationRole>(context));
            var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));

            roleManager.Create(new ApplicationRole("Admin"));
            roleManager.Create(new ApplicationRole("User"));

            var user = new ApplicationUser
            {
                Email = "mickelangello45ed@gmail.com",
                UserName = "mickelangello45ed@gmail.com"
            };


            userManager.Create(user, "Mk_210198");

            userManager.AddToRole(user.Id, "Admin");
            context.Users.Add(user);

            context.Files.Add(new FileEntity { Id = Guid.NewGuid(), Name = "File1", File = new byte[10], OwnerName = "mickelangello45ed@gmail.com", Size = 50, ContentType = "txt", IsShared = true, IsDeleted = false, CreationDate = DateTime.Now, Description = "File1", IsUploaded = true });
            context.Files.Add(new FileEntity { Id = Guid.NewGuid(), Name = "File2", File = new byte[10], OwnerName = "mickelangello45ed@gmail.com", Size = 50, ContentType = "txt", IsShared = false, IsDeleted = false, CreationDate = DateTime.Now, Description = "File2", IsUploaded = true });
            context.Files.Add(new FileEntity { Id = Guid.NewGuid(), Name = "File3", File = new byte[10], OwnerName = "mickelangello45ed@gmail.com", Size = 50, ContentType = "txt", IsShared = true, IsDeleted = true, CreationDate = DateTime.Now, Description = "File3", IsUploaded = true });

            base.Seed(context);
        }

    }
}
