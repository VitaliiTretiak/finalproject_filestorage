﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace File_Storage.DAL.Entities
{
    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole() : base()
        {
        }

        public ApplicationRole(string roleName) : base(roleName)
        {
        }

    }
}
