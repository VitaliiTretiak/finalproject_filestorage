﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;

namespace File_Storage.DAL.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public IList<FileEntity> FileIdentityModels { get; set; }
    }
}
