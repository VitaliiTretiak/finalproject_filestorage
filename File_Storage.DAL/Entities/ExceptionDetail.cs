﻿using System;

namespace File_Storage.DAL.Entities
{
    public class ExceptionDetail
    {
        public string Id { get; set; }
        public string ExceptionMessage { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string StackTrace { get; set; }
        public DateTime Date { get; set; }
    }
}
