﻿using File_Storage.DAL.EF;
using File_Storage.DAL.Entities;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace File_Storage.DAL.Identity
{
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
                : base(store)
        { }
        public ApplicationUserManager(ApplicationContext context, IUserStore<ApplicationUser> store) : base(store)
        {
            _context = context;
        }

        readonly ApplicationContext _context;

        public async Task Create(ApplicationUser user)
        {
            await Task.Run(() => _context.Users.Add(user));
        }
        public ApplicationUser Get(string id)
        {
            return _context.Users.Include(i => i.FileIdentityModels).Include(i => i.Roles).FirstOrDefault(i => i.Id == id);
        }

        public IEnumerable<ApplicationUser> GetAll()
        {
            return _context.Users.Include(i => i.FileIdentityModels).Include(i => i.Roles).ToList();
        }
        public void Update(ApplicationUser user)
        {
            var userEntity = _context.Users.Find(user.Id);

            if (userEntity != null)
            {
                userEntity.FileIdentityModels.Add(user.FileIdentityModels.LastOrDefault());
                _context.Entry(userEntity).State = EntityState.Modified;
                _context.SaveChanges();
            }
        }

        public void Delete(string id)
        {
            var userEntity = _context.Users.Find(Guid.Parse(id));

            if (userEntity != null)
            {
                _context.Users.Remove(userEntity);
                _context.SaveChanges();
            }
        }
    }
}
