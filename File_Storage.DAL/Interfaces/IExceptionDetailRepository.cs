﻿using File_Storage.DAL.Entities;
using System.Collections.Generic;

namespace File_Storage.DAL.Interfaces
{
    public interface IExceptionDetailRepository
    {
        void Create(ExceptionDetail item);
        IEnumerable<ExceptionDetail> GetAll();
    }
}
