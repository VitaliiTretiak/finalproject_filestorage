﻿using File_Storage.DAL.Entities;
using System.Threading.Tasks;

namespace File_Storage.DAL.Interfaces
{
    public interface IFileRepository : IRepository<FileEntity>
    {
        Task SoftDelete(string id);
    }
}
