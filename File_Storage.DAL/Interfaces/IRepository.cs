﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace File_Storage.DAL.Interfaces
{
    public interface IRepository<T> : IDisposable where T : class
    {
        IEnumerable<T> GetAll();
        T GetById(string id);
        Task Create(T item);
        void Update(T item);
        void Delete(string id);
    }

}
