﻿using File_Storage.DAL.Identity;
using System;
using System.Threading.Tasks;

namespace File_Storage.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        ApplicationUserManager UserManager { get; }
        ApplicationRoleManager RoleManager { get; }
        IFileRepository FileRepository { get; }
        IExceptionDetailRepository ExceptionDetailRepository { get; }
        Task Save();
    }
}
