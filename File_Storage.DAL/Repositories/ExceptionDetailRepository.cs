﻿using File_Storage.DAL.EF;
using File_Storage.DAL.Entities;
using File_Storage.DAL.Interfaces;
using System.Collections.Generic;

namespace File_Storage.DAL.Repositories
{
    public class ExceptionDetailRepository : IExceptionDetailRepository
    {
        readonly ApplicationContext _context;
        public ExceptionDetailRepository(ApplicationContext context)
        {
            _context = context;
        }
        public void Create(ExceptionDetail item)
        {
            _context.ExceptionDetails.Add(item);
        }

        public IEnumerable<ExceptionDetail> GetAll()
        {
            return _context.ExceptionDetails;
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
