﻿using File_Storage.DAL.EF;
using File_Storage.DAL.Entities;
using File_Storage.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace File_Storage.DAL.Repositories
{
    public class FileRepository : IFileRepository
    {
        readonly ApplicationContext _context;

        public FileRepository(ApplicationContext context)
        {
            _context = context;
        }

        public IEnumerable<FileEntity> GetAll()
        {
            return _context.Files;
        }

        public FileEntity GetById(string id)
        {
            return _context.Files.FirstOrDefault(i => i.Id.ToString() == id);
        }

        public async Task Create(FileEntity item)
        {
            await Task.Run(() => _context.Files.Add(item));
        }

        public void Update(FileEntity item)
        {
            var file = _context.Files.Find(item.Id);

            if (file != null)
            {
                file.Description = item.Description;
                file.IsUploaded = item.IsUploaded;
                file.IsDeleted = item.IsDeleted;
                _context.Entry(file).State = EntityState.Modified;
                _context.SaveChanges();
            }
        }

        public async Task SoftDelete(string id)
        {
            var file = await _context.Files.FindAsync(new Guid(id));

            if (file != null)
            {
                file.IsDeleted = true;
                file.DeleteDate = DateTime.Now;
                _context.Entry(file).State = EntityState.Modified;
            }
        }
        public void Delete(string id)
        {
            FileEntity item = _context.Files.Find(Guid.Parse(id));

            if (item != null)
            {
                _context.Files.Remove(item);
                _context.SaveChanges();
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }

}
