﻿using File_Storage.DAL.EF;
using File_Storage.DAL.Entities;
using File_Storage.DAL.Identity;
using File_Storage.DAL.Interfaces;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Threading.Tasks;

namespace File_Storage.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationContext _context;

        private readonly ApplicationUserManager _userManager;
        private readonly ApplicationRoleManager _roleManager;
        private readonly IFileRepository _fileRepository;
        private readonly IExceptionDetailRepository _exceptionDetailRepository;

        private bool _disposed = false;

        public UnitOfWork(string connectionString)
        {
            _context = new ApplicationContext(connectionString);
            _userManager = new ApplicationUserManager(_context, new UserStore<ApplicationUser>(_context));
            _roleManager = new ApplicationRoleManager(new RoleStore<ApplicationRole>(_context));
            _fileRepository = new FileRepository(_context);
            _exceptionDetailRepository = new ExceptionDetailRepository(_context);
        }

        public IFileRepository FileRepository
        {
            get { return _fileRepository; }
        }

        public IExceptionDetailRepository ExceptionDetailRepository
        {
            get { return _exceptionDetailRepository; }
        }

        public ApplicationUserManager UserManager
        {
            get { return _userManager; }
        }

        public ApplicationRoleManager RoleManager
        {
            get { return _roleManager; }
        }

        public async Task Save()
        {
            await _context.SaveChangesAsync();
        }

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _userManager.Dispose();
                    _roleManager.Dispose();
                    _fileRepository.Dispose();
                }
                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

}
