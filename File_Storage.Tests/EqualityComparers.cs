﻿using File_Storage.DAL.Entities;
using System.Collections.Generic;


namespace File_Storage.Tests
{
    internal class FileEqualityComparer : IEqualityComparer<FileEntity>
    {
        public bool Equals(FileEntity x, FileEntity y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id;
        }

        public int GetHashCode(FileEntity obj)
        {
            return obj.GetHashCode();
        }
    }
}
