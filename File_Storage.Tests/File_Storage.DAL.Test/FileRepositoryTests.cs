﻿using File_Storage.DAL.EF;
using File_Storage.DAL.Entities;
using File_Storage.DAL.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace File_Storage.Tests.File_Storage.DAL.Test
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestFixture]
    public class FileRepositoryTests
    {

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [Test]
        public void TestMethod1()
        {
            using (var context = new ApplicationContext("File_Storage.TestDb"))
            {
                var fileRepository = new FileRepository(context);

                var files = fileRepository.GetAll();

                Assert.That(files, Is.EqualTo(ExpectedFiles).Using(new FileEqualityComparer()));
            }
        }

        private static IEnumerable<FileEntity> ExpectedFiles =>
            new[]
            {
                new FileEntity { Id = Guid.NewGuid(), Name = "File1", File = new byte[10], OwnerName = "mickelangello45ed@gmail.com", Size = 50, ContentType = "txt", IsShared = true, IsDeleted = false, CreationDate = DateTime.Now, Description = "File1", IsUploaded = true },

                new FileEntity { Id = Guid.NewGuid(), Name = "File2", File = new byte[10], OwnerName = "mickelangello45ed@gmail.com", Size = 50, ContentType = "txt", IsShared = false, IsDeleted = false, CreationDate = DateTime.Now, Description = "File2", IsUploaded = true },

                new FileEntity { Id = Guid.NewGuid(), Name = "File3", File = new byte[10], OwnerName = "mickelangello45ed@gmail.com", Size = 50, ContentType = "txt", IsShared = true, IsDeleted = true, CreationDate = DateTime.Now, Description = "File3", IsUploaded = true }
            };
    }
}
