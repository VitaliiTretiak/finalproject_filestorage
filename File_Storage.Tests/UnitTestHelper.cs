﻿using File_Storage.DAL.EF;
using File_Storage.DAL.Entities;
using System;

namespace File_Storage.Tests
{
    internal static class UnitTestHelper
    {
        public static string ContextSetup(string connectionString)
        {
            using (var context = new ApplicationContext(connectionString))
            {
                SeedData(context);
            }

            return connectionString;
        }

        private static void SeedData(ApplicationContext context)
        {
            context.Files.Add(new FileEntity { Id = Guid.NewGuid(), Name = "File1", File = new byte[10], OwnerName = "mickelangello45ed@gmail.com", Size = 50, ContentType = "txt", IsShared = true, IsDeleted = false, CreationDate = DateTime.Now, Description = "File1", IsUploaded = true });
            context.Files.Add(new FileEntity { Id = Guid.NewGuid(), Name = "File2", File = new byte[10], OwnerName = "mickelangello45ed@gmail.com", Size = 50, ContentType = "txt", IsShared = false, IsDeleted = false, CreationDate = DateTime.Now, Description = "File2", IsUploaded = true });
            context.Files.Add(new FileEntity { Id = Guid.NewGuid(), Name = "File3", File = new byte[10], OwnerName = "mickelangello45ed@gmail.com", Size = 50, ContentType = "txt", IsShared = true, IsDeleted = true, CreationDate = DateTime.Now, Description = "File3", IsUploaded = true });
        }
    }
}
