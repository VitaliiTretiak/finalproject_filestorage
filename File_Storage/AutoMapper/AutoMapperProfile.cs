﻿using File_Storage.BLL.AutoMapper;
using File_Storage.BLL.DTO;
using File_Storage.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace File_Storage.AutoMapper
{
    public class AutoMapperProfile : AutoMapperProfileBll
    {
        public AutoMapperProfile() : base()
        {
            CreateMap<FileDto, FileViewModel>()
                .ReverseMap();

            CreateMap<UserDto, IdentityUser>()
                .ReverseMap();

            CreateMap<UserDto, UserViewModel>()
                .ReverseMap();
        }
    }
}