﻿using File_Storage.BLL.DTO;
using File_Storage.BLL.Interfaces;
using File_Storage.Models;
using Microsoft.Owin.Security;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace File_Storage.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        readonly IUserService _userService;

        readonly IFileService _fileService;

        public AccountController(IFileService fileService, IUserService userService)
        {
            _fileService = fileService;
            _userService = userService;
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }


        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                UserDto userDto = new UserDto { Email = model.Email, Password = model.Password };
                ClaimsIdentity claim = _userService.Authenticate(userDto);
                if (claim == null)
                {
                    ModelState.AddModelError("", "Неверный логин или пароль.");
                }
                else
                {
                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(claim);
                    return RedirectToLocal(returnUrl);
                }
            }
            return View(model);
        }

        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                UserDto userDto = new UserDto
                {
                    Email = model.Email,
                    Password = model.Password,
                    Role = "User"
                };

                _userService.Create(userDto);

                LoginViewModel loginModel = new LoginViewModel
                {
                    Email = model.Email,
                    Password = model.Password
                };

                Login(loginModel, null);
                return RedirectToAction("Index", "Home");
            }

            return View(model);
        }

        /// <summary>
        /// If user tries get access to admin page
        /// </summary>
        /// <returns></returns>
        public ActionResult NoAccess()
        {
            return View();
        }

        /// <summary>
        /// Redirect to url after login
        /// </summary>
        /// <param name="returnUrl">Return url</param>
        /// <returns></returns>
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

    }
}