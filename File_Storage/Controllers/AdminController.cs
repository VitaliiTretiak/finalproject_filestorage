﻿using AutoMapper;
using File_Storage.BLL.DTO;
using File_Storage.BLL.Interfaces;
using File_Storage.BLL.Validation;
using File_Storage.Filters;
using File_Storage.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace File_Storage.Controllers
{

    [AdminAction]
    public class AdminController : Controller
    {
        readonly IUserService _userService;
        readonly IFileService _fileService;
        readonly IExceptionService _exceptionService;
        readonly IMapper _mapper;

        public AdminController(IFileService fileService, IUserService userService, IExceptionService exceptionService, IMapper mapper)
        {
            _fileService = fileService;
            _userService = userService;
            _exceptionService = exceptionService;
            _mapper = mapper;
        }

        /// <summary>
        /// Admin menu
        /// </summary>
        public ActionResult Menu()
        {
            return View();
        }

        /// <summary>
        /// Shows all  users
        /// </summary>
        public ActionResult ShowAllUsers()
        {
            var userRoles = new Dictionary<string, IList<string>>();
            var users = _mapper.Map<IEnumerable<UserDto>, List<UserViewModel>>(_userService.GetAll().ToList());

            for (int i = 0; i < users.Count; i++)
            {
                string id = users[i].Id;
                userRoles[id] = _userService.GetRoles(id);
            }

            ViewBag.UserRoles = userRoles;

            return PartialView(users);
        }

        /// <summary>
        /// Shows all files of user
        /// </summary>
        /// <param name="id">User`s id</param>
        public ActionResult ShowFilesOfUser(string id)
        {
            var files = _userService.GetById(id).FileIdentityModels.ToList();

            return PartialView(_mapper.Map<List<FileDto>, List<FileViewModel>>(files)
                .Where(i => !i.IsDeleted)
                .OrderByDescending(i => i.CreationDate)
                .ToList());
        }

        /// <summary>
        /// Show log of all errors
        /// </summary>
        public ActionResult ShowErrorLogs()
        {
            var errors = _exceptionService.GetAll()
                .OrderByDescending(i => i.Date)
                .ToList();

            return PartialView(errors);
        }

        /// <summary>
        /// Deletes user
        /// </summary>
        /// <param name="id">User`s id</param>
        public ActionResult DeleteUser(string id)
        {
            var user = _userService.GetById(id);
            var role = _userService.GetRoles(id).FirstOrDefault();


            if (role == "Admin")
                throw new FileStorageException("Admin account cannot be deleted");

            var userFiles = _fileService.GetAll().Where(i => i.OwnerName == user.UserName)
                .ToList();

            foreach (var file in userFiles)
            {
                _fileService.Delete(file.Id);
            }

            _userService.Delete(id);

            return RedirectToAction("Menu", "Admin");
        }
    }
}