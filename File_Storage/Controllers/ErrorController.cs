﻿using File_Storage.BLL.Interfaces;
using System.Linq;
using System.Web.Mvc;

namespace File_Storage.Controllers
{
    public class ErrorController : Controller
    {
        readonly IExceptionService _exceptionService;

        public ErrorController(IExceptionService exceptionService)
        {
            _exceptionService = exceptionService;
        }

        /// <summary>
        /// Handles error 404
        /// </summary>
        public ActionResult NotFound()
        {
            return View();
        }

        /// <summary>
        /// Handles all errors except 404
        /// </summary>
        public ActionResult Error()
        {
            return View();
        }

        /// <summary>
        /// Shows details of error
        /// </summary>
        public ActionResult ShowDetails()
        {
            var exception = _exceptionService.GetAll().OrderByDescending(i => i.Date).FirstOrDefault();

            ViewBag.ExceptionMessage = exception.ExceptionMessage;

            return PartialView();
        }
    }
}