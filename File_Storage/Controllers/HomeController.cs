﻿using AutoMapper;
using File_Storage.BLL.DTO;
using File_Storage.BLL.Interfaces;
using File_Storage.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace File_Storage.Controllers
{
    public class HomeController : Controller
    {
        readonly IUserService _userService;
        readonly IFileService _fileService;
        readonly IExceptionService _exceptionService;
        readonly IMapper _mapper;

        public HomeController(IFileService fileService, IUserService userService, IExceptionService exceptionService, IMapper mapper)
        {
            _fileService = fileService;
            _userService = userService;
            _exceptionService = exceptionService;
            _mapper = mapper;
        }

        /// <summary>
        /// Main page
        /// </summary>
        public ActionResult Index()
        {
            IEnumerable<FileDto> fileDtos = _fileService.GetAll()
                .Where(i => i.IsShared && !i.IsDeleted)
                .OrderByDescending(i => i.CreationDate);

            var files = _mapper.Map<IEnumerable<FileDto>, List<FileViewModel>>(fileDtos);

            return View(files);
        }

        public ActionResult Create()
        {
            return PartialView();
        }

        /// <summary>
        /// Creates new file
        /// </summary>
        /// <param name="file">File model</param>
        /// <param name="uploadFile">Uploaded file</param>
        [HttpPost]
        public async Task<ActionResult> Create(FileViewModel file, HttpPostedFileBase uploadFile)
        {
            var user = _userService.GetById(User.Identity.GetUserId());

            if (uploadFile != null)
            {
                byte[] fileData = null;

                using (var binaryReader = new BinaryReader(uploadFile.InputStream))
                {
                    fileData = binaryReader.ReadBytes(uploadFile.ContentLength);
                    file.Size = (long)Math.Ceiling(Math.Round((double)uploadFile.InputStream.Length / 1024, 1));
                }

                var fileName = uploadFile.FileName.Split('.');

                if (uploadFile.FileName.Length > 20)
                    file.Name = fileName[0].Substring(0, 20) + $".{fileName[1]}";
                else
                    file.Name = uploadFile.FileName;

                if (user != null)
                {
                    file.OwnerName = user.UserName;
                    file.UserId = user.Id;
                }
                else
                    file.OwnerName = "Guest";


                file.File = fileData;
                file.ContentType = uploadFile.ContentType;
                file.Description = fileName[0];
                file.CreationDate = DateTime.Now;
                file.DeleteDate = DateTime.Now.AddYears(5);
                file.Id = Guid.NewGuid().ToString();

                var fileDto = _mapper.Map<FileViewModel, FileDto>(file);

                if (user != null)
                {
                    user.FileIdentityModels.Add(fileDto);
                    _userService.Update(user);
                }
                else
                {
                    await _fileService.Create(fileDto);
                }
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Downloads file
        /// </summary>
        /// <param name="id">File`s id</param>
        public ActionResult GetFile(string id)
        {
            FileDto file = _fileService.GetById(id);

            file.IsUploaded = true;

            _fileService.Update(file);

            byte[] data = file.File;
            string fileName = file.Name;
            string contentType = file.ContentType;

            return File(data, contentType, fileName);
        }

        /// <summary>
        /// Shows file`s properties
        /// </summary>
        /// <param name="id">File`s d</param>
        /// <param name="controllerNameFrom">Controller name where from file`s properties need to show</param>
        public ActionResult ShowProperties(string id, string controllerNameFrom)
        {
            ViewBag.ControllerName = controllerNameFrom;

            return PartialView(_mapper.Map<FileDto, FileViewModel>(_fileService.GetById(id)));
        }

        /// <summary>
        /// Updates description of file
        /// </summary>
        /// <param name="form">Form</param>
        /// <param name="id">File`s id</param>
        [HttpPost]
        public ActionResult ShowProperties(FormCollection form, string id)
        {
            var file = _fileService.GetById(id);

            file.Description = form["description"];

            _fileService.Update(file);
            ViewBag.ControllerName = "User";

            return PartialView(_mapper.Map<FileDto, FileViewModel>(file));
        }

        /// <summary>
        /// Shows recent files
        /// </summary>
        /// <returns></returns>
        public ActionResult GetRecentFiles()
        {
            var files = _fileService.GetAll();

            int count = files.Count();

            if (count > 10)
                count -= 10;
            else
                count = 0;

            return View(_mapper.Map<IEnumerable<FileDto>, List<FileViewModel>>(files)
                .Where(i => !i.IsDeleted)
                .OrderBy(i => i.CreationDate)
                .Skip(count)
                .Take(10)
                .ToList());
        }

        /// <summary>
        /// Searches needed files
        /// </summary>
        /// <param name="collection">Search input</param>
        [HttpPost]
        public ActionResult Search(FormCollection collection)
        {
            var searchItem = collection["Search"];

            if (searchItem.Length == 0)
            {
                ViewBag.Error = "Cannot be empty";
                return PartialView();
            }

            List<FileDto> fileDtos;
            List<FileViewModel> fileModels;

            if (User.IsInRole("Admin"))
            {
                fileDtos = _fileService.GetAll()
                    .Where(i => !i.IsDeleted && (i.Name.Contains(searchItem)
                        || i.OwnerName.Contains(searchItem)
                        || i.Description.Contains(searchItem)
                        || i.ContentType.Contains(searchItem)))
                    .ToList();

                fileModels = _mapper.Map<List<FileDto>, List<FileViewModel>>(fileDtos);
            }
            else if (User.IsInRole("User"))
            {
                fileDtos = _userService.GetById(User.Identity.GetUserId()).FileIdentityModels
                    .Where(i => !i.IsDeleted && (i.Name.Contains(searchItem)
                        || i.OwnerName.Contains(searchItem)
                        || i.Description.Contains(searchItem)
                        || i.ContentType.Contains(searchItem)))
                    .ToList();

                fileDtos.AddRange(_fileService.GetAll()
                    .Where(i => (i.IsShared
                        || i.OwnerName == "Guest")
                        && !i.IsDeleted &&
                        (i.Name.Contains(searchItem)
                            || i.OwnerName.Contains(searchItem)
                            || i.Description.Contains(searchItem)
                            || i.ContentType.Contains(searchItem)))
                    .ToList());

                fileModels = _mapper.Map<List<FileDto>, List<FileViewModel>>(fileDtos);
            }
            else
            {
                fileDtos = _fileService.GetAll()
                    .Where(i => (i.IsShared
                        || i.OwnerName == "Guest")
                        && !i.IsDeleted &&
                        (i.Name.Contains(searchItem)
                            || i.OwnerName.Contains(searchItem)
                            || i.Description.Contains(searchItem)
                            || i.ContentType.Contains(searchItem)))
                    .ToList();

                fileModels = _mapper.Map<List<FileDto>, List<FileViewModel>>(fileDtos);
            }



            return PartialView(fileModels);
        }
    }
}