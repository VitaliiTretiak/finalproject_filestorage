﻿using AutoMapper;
using File_Storage.BLL.DTO;
using File_Storage.BLL.Interfaces;
using File_Storage.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace File_Storage.Controllers
{
    [Authorize(Roles = "Admin, User")]
    public class UserController : Controller
    {
        readonly IUserService _userService;
        readonly IFileService _fileService;
        readonly IMapper _mapper;

        public UserController(IFileService fileService, IUserService userService, IMapper mapper)
        {
            _fileService = fileService;
            _userService = userService;
            _mapper = mapper;
        }

        /// <summary>
        /// Menu of user account
        /// </summary>
        public ActionResult Menu()
        {
            return View();
        }

        /// <summary>
        /// Shows user`s files
        /// </summary>
        /// <param name="id">User`s id</param>
        public ActionResult ShowUserFiles(string id)
        {
            var files = _userService.GetById(id).FileIdentityModels.ToList();

            return PartialView(_mapper.Map<List<FileDto>, List<FileViewModel>>(files)
                .Where(i => !i.IsDeleted)
                .OrderByDescending(i => i.CreationDate));
        }

        /// <summary>
        /// Moves file to recycle bin
        /// </summary>
        /// <param name="id">File`s id</param>
        public async Task<ActionResult> SoftDeleteFile(string id)
        {
            await _fileService.SoftDelete(id);

            return RedirectToAction("Menu");
        }

        /// <summary>
        /// Delete file from recycle bin or from admin panel
        /// </summary>
        /// <param name="id">File`s id</param>
        public ActionResult Delete(string id)
        {
            _fileService.Delete(id);
            var userId = GetUserId();

            if (User.IsInRole("Admin"))
                return RedirectToAction("Menu", "Admin");
            else
                return RedirectToAction("ShowDeletedFiles", new { id = userId });
        }

        /// <summary>
        /// Moves file from recycle bin to account
        /// </summary>
        /// <param name="id">File`s id</param>
        public ActionResult Restore(string id)
        {
            FileDto file = _fileService.GetById(id);
            file.IsDeleted = false;
            var userId = GetUserId();

            _fileService.Update(file);

            return RedirectToAction("ShowDeletedFiles", new { id = userId });
        }

        /// <summary>
        /// Shows deleted files
        /// </summary>
        /// <param name="id">User`s id</param>
        /// <returns></returns>
        public ActionResult ShowDeletedFiles(string id)
        {
            var files = _userService.GetById(id).FileIdentityModels.ToList();

            return View(_mapper.Map<List<FileDto>, List<FileViewModel>>(files)
                .Where(i => i.IsDeleted)
                .OrderByDescending(i => i.CreationDate)
                .ToList());
        }

        /// <summary>
        /// Gets current user`s id
        /// </summary>
        /// <returns>Users`s id</returns>
        private string GetUserId()
        {
            return _userService.GetAll().FirstOrDefault(i => i.UserName == User.Identity.Name).Id;
        }
    }
}