﻿using System.Web.Mvc;
using System.Web.Routing;

namespace File_Storage.Filters
{
    public class AdminActionAttribute : FilterAttribute, IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            
        }
        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsAuthenticated)
            {
                if (!filterContext.HttpContext.User.IsInRole("Admin"))
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                        {
                            { "action", "NoAccess" },
                            { "controller", "Account" }
                        }
                    );
                }
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                    {
                        { "action", "Login" },
                        { "controller", "Account" }
                    }
                );
            }
        }
    }
}