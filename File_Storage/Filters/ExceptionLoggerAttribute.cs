﻿using File_Storage.BLL.DTO;
using File_Storage.BLL.Interfaces;
using File_Storage.BLL.Ninject;
using Ninject;
using System;
using System.Web.Mvc;

namespace File_Storage.Filters
{
    public class ExceptionLoggerAttribute : FilterAttribute, IExceptionFilter
    {
        readonly IExceptionService _exceptionService;

        public ExceptionLoggerAttribute() : this(new StandardKernel(new ContainerBuilder(null)).Get<IExceptionService>()) { }

        public ExceptionLoggerAttribute(IExceptionService exceptionService)
        {
            _exceptionService = exceptionService;
        }
        public void OnException(ExceptionContext filterContext)
        {
            ExceptionDetailDto exceptionDetail = new ExceptionDetailDto()
            {
                ExceptionMessage = filterContext.Exception.Message,
                StackTrace = filterContext.Exception.StackTrace,
                ControllerName = filterContext.RouteData.Values["controller"].ToString(),
                ActionName = filterContext.RouteData.Values["action"].ToString(),
                Date = DateTime.Now,
                Id = Guid.NewGuid().ToString()
            };

            _exceptionService.Create(exceptionDetail);
        }
    }
}