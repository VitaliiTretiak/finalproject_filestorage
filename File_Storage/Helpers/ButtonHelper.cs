﻿using System.Web.Mvc;

namespace File_Storage.Helpers
{
    public static class ButtonHelper
    {
        public static MvcHtmlString CreateButton(this HtmlHelper html, string buttonName, object htmlAttributesA = null, object htmlAttributesI = null)
        {
            TagBuilder a = new TagBuilder("a");
            TagBuilder i = new TagBuilder("i");

            a.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributesA));
            i.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributesI));
            a.InnerHtml += i.ToString() + buttonName;

            return MvcHtmlString.Create(a.ToString());
        }
    }
}