﻿using System;

namespace File_Storage.Models
{
    public class FileViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public byte[] File { get; set; }
        public string OwnerName { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime DeleteDate { get; set; }
        public string ContentType { get; set; }
        public string Description { get; set; }
        public long Size { get; set; }
        public bool IsUploaded { get; set; }
        public bool IsShared { get; set; }
        public bool IsDeleted { get; set; }
        public string UserId { get; set; }
    }
}